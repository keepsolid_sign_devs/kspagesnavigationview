#ifndef DOCUMENTPREVIEWPAGES_H
#define DOCUMENTPREVIEWPAGES_H

#include <QAbstractListModel>
#include <QUrl>

class DocumentPreviewPages : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit DocumentPreviewPages(QObject *parent = nullptr);

    Q_INVOKABLE bool isClueNeeded(int index);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    enum DocumentPreviewRoles {

        DocumentPreviewRoleImage = Qt::UserRole + 1,
    };
public slots:
    void setActiveIndex(int index);
    int activeIndex();
signals:
    void activeIndexChanged();
protected:
    QHash<int,QByteArray> roleNames() const override;
private:
    QList<QString> _pagesPreview;
    int _activeIndex = 0;
};

#endif // DOCUMENTPREVIEWPAGES_H
