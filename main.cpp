#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <DocumentPreviewPages.h>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<DocumentPreviewPages>("DocumentPreviewPages", 1, 0, "DocumentPreviewPages");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
