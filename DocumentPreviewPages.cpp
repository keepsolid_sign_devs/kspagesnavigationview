#include "DocumentPreviewPages.h"
#include <QTimer>

DocumentPreviewPages::DocumentPreviewPages(QObject *parent)
    : QAbstractListModel(parent)
{
    QString imagePath = QString("111.png");
    for (int i = 0; i < 20; i++) {

        _pagesPreview.append(imagePath);
    }
}

bool DocumentPreviewPages::isClueNeeded(int index) {

    //logic for retrieving clue info
    return index % 2 == 0;
}

void DocumentPreviewPages::setActiveIndex(int index) {

    if (_activeIndex != index && index >= 0 && index < _pagesPreview.size()) {

        _activeIndex = index;
        emit activeIndexChanged();
    }
}

int DocumentPreviewPages::activeIndex() {

    return _activeIndex;
}

int DocumentPreviewPages::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return _pagesPreview.count();
}

QHash<int,QByteArray> DocumentPreviewPages::roleNames() const {

    QHash<int, QByteArray> roles;
    roles[DocumentPreviewRoleImage] = "preview";
    return roles;
}

QVariant DocumentPreviewPages::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    switch (role) {
    case DocumentPreviewRoleImage:

        return _pagesPreview[index.row()];
        break;
    default:
        break;
    }
    return QVariant();
}
