import QtQuick 2.0
import DocumentPreviewPages 1.0

Item {

    property int panelWidth: 170

    id: pagesNavigationView
    anchors {
        top: parent.top
        bottom: parent.bottom
        left: parent.left
    }
    width: panelWidth

    states: [
        State {
            name: "Opened"
            when: xPointInRoot() > panelWidth / 2
            PropertyChanges {
                target: pagesNavigationView
                width: panelWidth
            }
        },
        State {
            name: "Closed"
            when: xPointInRoot() < panelWidth / 2
            PropertyChanges {
                target: pagesNavigationView
                width: availabilityMouseArea.width
            }
        }
    ]


    function xPointInRoot() {

        var pointInRoot = availabilityMouseArea.mapToItem(null,
                                                          availabilityMouseArea.mouseX,
                                                          availabilityMouseArea.mouseY);
        return pointInRoot.x;
    }

    Rectangle {

        id: backgroundRectangle
        color: "#f4f4f4"
        anchors.fill: parent
        ListView {

            id: pagesListView
            anchors.fill: parent
            anchors.topMargin: 25
            anchors.bottomMargin: 25
            model: DocumentPreviewPages {

                id: documentPreviewsManager
                onActiveIndexChanged: {

                    console.log(activeIndex());
                    pagesListView.currentIndex = activeIndex();
                }
            }
            delegate: DocumentPagePreviewCell {

                id: cell
                backgroundColor: backgroundRectangle.color
                cellImage: Qt.resolvedUrl(model.preview)
                cellIndex: model.index + 1
            }
            spacing: 15
        }
    }
    MouseArea {

        id: availabilityMouseArea
        anchors {

            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }
        width: 5
        cursorShape: Qt.SizeHorCursor
        Rectangle {

            anchors.fill: parent
            color: "green"
            Image {
                id: resizeImage
                anchors.centerIn: parent
                width: parent.width
                height: 30//value has to be changed to appropriate image
                source: "111.png"
            }
        }
        onDoubleClicked:  {

            if (pagesNavigationView.state == "Opened") {

                pagesNavigationView.state = "Closed";
            } else {

                pagesNavigationView.state = "Opened";
            }
        }
    }
}
