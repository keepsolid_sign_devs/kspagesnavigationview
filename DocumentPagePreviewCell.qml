import QtQuick 2.0
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

Item {

    id: previewPageCell
    width: parent.width
    height: 146

    property color backgroundColor

    property alias cellImage: image.source
    property alias cellIndex: textIndex.text

    Rectangle {

        anchors.fill: parent
        color: backgroundColor

        Rectangle {

            id: cellBackground
            color:  pagesListView.currentIndex == index ? "#daebf9" : "#ffffff"
            border.width: pagesListView.currentIndex == index ? 1 : 0
            border.color: pagesListView.currentIndex == index ? "#322980cc" : ""
            anchors.rightMargin: 35
            anchors.leftMargin: 35
            anchors.fill: parent

            Rectangle {

                id: visualClueRect
                anchors  {

                    top: parent.top
                    topMargin: 16
                    right: parent.right
                    rightMargin: -width / 2
                }
                width: 16
                height: 10
                visible: documentPreviewsManager.isClueNeeded(index)
                LinearGradient {

                    anchors.fill: parent
                    start: Qt.point(x, y);
                    end: Qt.point(width, height);
                    gradient: Gradient {

                        id: openedGradient
                        GradientStop {
                            position: 0
                            color: "#33a0ff"
                        }

                        GradientStop {
                            position: 1
                            color: "#2168a6"
                        }
                    }
                }
            }

            Rectangle {

                id: shadowRect
                anchors.fill: image
                border.width: 1
                border.color: "#ebebeb"
            }

            Image {
                id: image
                height: 110
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.rightMargin: 8
                anchors.leftMargin: 8
                anchors.topMargin: 8
                fillMode: Image.PreserveAspectFit
                BusyIndicator {

                    anchors.centerIn: image
                    running: image.status == Image.Loading
                }
                //add loader
            }

            Text {
                id: textIndex
                text: qsTr("1")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.rightMargin: 8
                anchors.leftMargin: 8
                anchors.bottomMargin: 5
                anchors.top: image.bottom
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.topMargin: 6
                font.pixelSize: 9
                //font.family: "OpenSans-Regular" has to be setted while integration
                color: "#444444"
            }

            MouseArea {

                anchors.fill: parent
                onClicked: {

                    documentPreviewsManager.setActiveIndex(index);
                }
            }
        }
    }
}
